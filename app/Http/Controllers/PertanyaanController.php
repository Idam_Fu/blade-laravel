<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    //
    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        // dd($pertanyaan);
        return view('pages.pertanyaan',compact('pertanyaan'));
    }

    public function create(){
        return view('forms.create');
    }

    public function store(Request $request){
        // dd($request->request);
        $request->validate([
            'judul' =>'required',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            'judul' => $request->input('judul'),
            'isi' => $request->input('isi')
        ]);
        return redirect('/pertanyaan')->with('success_add_data_pertanyaan', 'Berhasil Menambahkan Data !!!');
    }

    public function show($id){
        $show = DB::table('pertanyaan')->where('id',$id)->first();
        // dd($show);
        return view('pages.show',compact('show'));
    }

    public function edit($id){
        
        $edit = DB::table('pertanyaan')->where('id',$id)->first();
        return view('forms.edit',compact('edit'));
    }

    public function update($id,Request $request){
        //
        $request->validate([
            'judul' =>'required',
            'isi' => 'required'
        ]);
        $update = DB::table('pertanyaan')->where('id',$id)
                                        ->update([
                                            'judul' => $request['judul'],
                                            'isi' => $request['isi']
                                        ]);

        return redirect('/pertanyaan')->with('success_update_data_pertanyaan', 'Berhasil Mengupdate Data !!!');
    }

    public function destroy($id){
        $delete = DB::table('pertanyaan')->where('id',$id)->delete();

        return redirect('/pertanyaan')->with('delete_data_pertanyaan', 'Data Berhasil di Hapus !!!');
    }
}
