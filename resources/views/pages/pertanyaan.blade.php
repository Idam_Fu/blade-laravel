@extends('layouts.master')

@section('title','Lihat Pertanyaan')
@section('content')

<div class="card">
    <!-- <div class="card-header">
        <h3 class="card-title">DataTable with minimal features & hover style</h3>
    </div> -->
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('success_add_data_pertanyaan'))
            <div class="alert alert-success">{{session('success_add_data_pertanyaan')}}</div>
        @elseif(session('success_update_data_pertanyaan'))
            <div class="alert alert-success">{{session('success_update_data_pertanyaan')}}</div>
        @elseif(session('delete_data_pertanyaan'))
            <div class="alert alert-success">{{session('delete_data_pertanyaan')}}</div>
        @endif
        <a href="/pertanyaan/create" class="btn btn-primary mb-3">Create new pertanyaan</a>
    <table id="example1" class="table table-bordered table-hover">
        <thead>
        <tr>
        <th>#</th>
        <th>Judul</th>
        <th>Isi</th>
        <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse($pertanyaan as $key => $tanya)
        <tr>
        <td>{{$key + 1 }}</td>
        <td>{{$tanya->judul}}</td>
        <td>{{$tanya->isi}}</td>
        <td style="display:flex">
            <a href="/pertanyaan/{{$tanya->id}}" class="btn btn-info btn-sm">Show</a>
            <a href="/pertanyaan/{{$tanya->id}}/edit" class="btn btn-warning btn-sm ml-1">Edit</a>
            <form action="/pertanyaan/{{$tanya->id}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger btn-sm ml-1" value="Delete">
            </form>
        </td>
        </tr>
        @empty
        <tr>
            <td colspan="4" class="text-center">No Data In Here</td>
        </tr>
        @endforelse
        </tbody>
    </table>
    </div>
    <!-- /.card-body -->
</div>
            <!-- /.card -->

@endsection

@push('script')

    <script src="{{ asset('/assets/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>

@endpush