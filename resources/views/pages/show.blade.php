@extends('layouts.master')

@section('title','Informasi Lengkap')
@section('content')

<div class="card">
  <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
  <div class="card-body">
    <h1 class="card-title">{{$show->judul}}</h1>
    <p class="card-text">{{$show->isi}}</p>
    <a href="/pertanyaan" class="btn btn-primary">Go Back</a>
  </div>
</div>

@endsection