@extends('layouts.master')

@section('title','Edit Pertanyaan')

@section('content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title"><small>Masukkan Pertanyaan yang ingin anda ajukkan pada id ke {{$edit->id}}</small></h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/{{$edit->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="judul">Judul:</label>
                    <input type="text" class="form-control" id="judul" value="{{ old( 'judul', $edit->judul) }}" placeholder="Masukkan judul" name="judul">
                    @error('judul')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
            
            <div class="form-group">
                <label for="isi">Isi:</label>
                    <textarea class="form-control rounded-0" id="isi" rows="8" name="isi" placeholder="Masukkan isi pertanyaan">{{ old('isi', $edit->isi) }}</textarea>
                    @error('isi')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
        </div>
    <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
        </div>
    </form>
</div>


@endsection